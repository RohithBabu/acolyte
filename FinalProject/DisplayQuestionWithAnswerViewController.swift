//
//  DisplayQuestionWithAnswerViewController.swift
//  FinalProject
//
//  Created by Moturu,Chaitanya Kiran on 4/10/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse
import Bolts

class DisplayQuestionWithAnswerViewController: UIViewController {
    var category:String = ""
    var categoryQuestion:String = ""

    var bikes:[PFObject] = []
    var bikes1:[String] = []
    var number = 0
    var k:Character = "a"
    @IBOutlet weak var outputQuestion: UILabel!
    @IBOutlet weak var outputAnswers: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadView()
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var inputAnswer: UITextField!
    override func viewWillAppear(_ animated: Bool) {
        
        outputQuestion.text = self.categoryQuestion
        displayOutputAskedQuestions()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickPost(_ sender: Any) {
        let bike = PFObject(className: "categoryQA")
      
        bike["categoryname"] = self.category
        bike["categoryQuestion"] = self.categoryQuestion
        bike["categoryAnswer"] = inputAnswer.text
        bike.saveInBackground(block: { (success, error) -> Void in
            self.displayAlertWithTitle("Success!", message:"Your Answer added")
        })
        inputAnswer.text = ""
        displayOutputAskedQuestions()
    }
    func displayOutputAskedQuestions(){
        self.outputAnswers.text = "Answers :"
        let query = PFQuery(className:"categoryQA")

        var count = 0
            self.bikes1 = []
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.

                self.bikes = objects!
                for name in objects! {
                    if name["categoryQuestion"] as? String == self.categoryQuestion{
                        self.bikes1.append(name["categoryAnswer"] as! String)}
                    
                }
                
                for i in self.bikes1{
                    count = count+1
                     self.outputAnswers.text =  self.outputAnswers.text + "\n\(i)"
                   
                }
                
            } else {
                // Log details of the failure
                self.displayAlertWithTitle("Oops", message: "\(error!) \(error!._userInfo)")
            }
        })
        
    }
    
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
