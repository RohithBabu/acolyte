//
//  SecondViewController.swift
//  FinalProject
//
//  Created by Sadhu,Rohith Babu on 3/5/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse
import Bolts

class SecondViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate  {
    var acolyte = ["Bikes","Watch","Cars","Computer","Microwave","Mobiles"]
    var bikes:[PFObject] = []
    var bikes1:[String] = []
    
   // var acolyteImages = [#imageLiteral(resourceName: "bike"),#imageLiteral(resourceName: "watch"),#imageLiteral(resourceName: "car"),#imageLiteral(resourceName: "computer accessories"),#imageLiteral(resourceName: "microwave"),#imageLiteral(resourceName: "mobile")]
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate=self
        tableView.dataSource=self
        // Do any additional setup after loading the view, typically from a nib.
        let query = PFQuery(className: "category")
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                
                self.bikes = objects!
                for name in objects! {
                    
                    self.bikes1.append(name["categoryname"] as! String)
                    
                }
                self.tableView.reloadData()
                print(self.bikes.count)
                
            }
            else {
                
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func logoutButton(_ sender: Any) {
    }
    @IBAction func backToDashboard(unwind : UIStoryboardSegue){
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bikes1.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=UITableViewCell()
        //print("---------\(indexPath.row)")
        //print((bikes1[indexPath.row]))
        //cell.textLabel?.text="\(acolyte[indexPath.row])"
        cell.textLabel?.text="\(bikes1[indexPath.row])"
        //print("----------------------\(a[indexPath.row])")
        //tableView.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "acolyte", sender: bikes1[indexPath.row])
    }
    override func prepare(for segue: UIStoryboardSegue, sender:Any? ) {
        if(segue.identifier == "acolyte"){
        let c=segue.destination as! BikesPageViewController
            c.number=sender as! String}
    }
}

