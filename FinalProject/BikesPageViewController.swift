//
//  BikesPageViewController.swift
//  FinalProject
//
//  Created by Moturu,Chaitanya Kiran on 3/30/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse
import Bolts

class BikesPageViewController: UIViewController{
    
   
 
    var number:String = ""
    
    @IBOutlet weak var questionBike: UITextField!
    
    var bikes:[PFObject] = []
        var bikes1:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let query = PFQuery(className: "category")
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                
                self.bikes = objects!
                for name in objects! {
                    
                    self.bikes1.append(name["categoryname"] as! String)
                    
                }
               // self.tableView.reloadData()
                print(self.bikes.count)
                
            }
            else {
                
            }
        })
        // Do any additional setup after loading the view.
        self.loadView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        outputDisplayQuestions.textContainerInset = UIEdgeInsetsMake(0, 50, 0, 0);
         displayOutputAskedQuestions()
    }
    
    @IBAction func backToQuestions(unwind : UIStoryboardSegue){
        
    }
   
    
  
    @IBAction func askClick(_ sender: Any) {
        let bike = PFObject(className: "categoryQ")
        
        let tempCategory = number
        
        bike["category"] = tempCategory
        bike["categoryQuestion"] = questionBike.text
        
        bike.saveInBackground(block: { (success, error) -> Void in
            print("question has been saved.")
        })
        questionBike.text = ""
        
            displayOutputAskedQuestions()
    }
    @IBOutlet weak var outputDisplayQuestions: UITextView!
    
    func displayOutputAskedQuestions(){
        self.outputDisplayQuestions.text = "Asked Questions :"
        let query = PFQuery(className: "categoryQ")
        //var k = self.bikes1.index(after: number)
        let tempCategory = number
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                //self.displayAlertWithTitle("Success!", message:"Retrieved \(objects!.count) scores.")
                self.bikes = objects!
                print(self.bikes.count)
                // Do something with the found objects
                //var j=0
                for i in self.bikes{
                    if(i["category"] as! String == tempCategory){
                    self.outputDisplayQuestions.text =  self.outputDisplayQuestions.text + "\n" + "\(i["categoryQuestion"]!)"
                    //self.a[j] = i["questionBike"]! as! String
                    }
                }
                //self.bikesTabelDisplay.reloadData()
            } else {
                // Log details of the failure
                self.displayAlertWithTitle("Oops", message: "\(error!) \(error!._userInfo)")
            }
        })
        
    }//
   
  
    
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let s = segue.destination as! QuestionAndAnswersViewController
       s.category = number
      
        
    }

    
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
