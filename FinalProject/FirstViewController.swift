//
//  FirstViewController.swift
//  FinalProject
//
//  Created by Sadhu,Rohith Babu on 3/5/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse

class FirstViewController: UIViewController {

    
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
    }
    @IBAction func login(_ sender: AnyObject) {
        if(usernameTF.text! != "" && passwordTF.text! != ""){
        PFUser.logInWithUsername(inBackground: usernameTF.text!, password: passwordTF.text!, block:{(user, error) -> Void in
            if error != nil{
                print("error")
            }
            else {
                // Everything went alright here
                self.displayAlertWithTitle("Success!", message:"Login successful")
                //self.performSegue(withIdentifier: "loginToDashboard", sender: nil)
                
            }
        })}
        else{
             self.displayAlertWithTitle("Unsuccess!", message:"Username or Password Must be entered")
        }
    }
    @IBAction func register(_ sender: AnyObject) {
        // Defining the user object
        //self.performSegue(withIdentifier: "loginToRegister", sender: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func backToLogin(unwind : UIStoryboardSegue){
        PFUser.logOut()
    }
}

