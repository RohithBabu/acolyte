//
//  MapViewViewController.swift
//  FinalProject
//
//  Created by Moturu,Chaitanya Kiran on 4/26/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import MapKit
import Parse
import Bolts

class MapViewViewController: UIViewController {
    var latitude:CLLocationDegrees = 0.0
    var longitude:CLLocationDegrees = 0.0
    var title1:String = ""
    var subtitle:String = ""
    var bikes:[PFObject] = []
    var bikes1:[String] = []
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 40.357294, longitude: -94.872736)// .. populate your center
        let latitudinalMeters: CLLocationDistance = 750
        let longitudinalMeters: CLLocationDistance = 750
        let region = MKCoordinateRegionMakeWithDistance(coordinate, latitudinalMeters, longitudinalMeters)
        self.mapView.setRegion(region, animated: false)
        
        let here:MKPointAnnotation = MKPointAnnotation()
        here.title = "Maryville"
        here.subtitle = "MO"
        here.coordinate = CLLocationCoordinate2D(latitude: 40.357294, longitude: -94.872736)

        let query = PFQuery(className:"map")
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                
                self.bikes = objects!
                
                for location in self.bikes {
                    let annotation = MKPointAnnotation()
                    annotation.title = location["title"] as? String
                    annotation.subtitle = location["subtitle"] as? String
                    annotation.coordinate = CLLocationCoordinate2D(latitude: location["latitude"] as! CLLocationDegrees, longitude: location["longitude1"] as! CLLocationDegrees)
                    self.mapView.addAnnotation(annotation)
                }
                
            } else {
                // Log details of the failure
                //self.displayAlertWithTitle("Oops", message: "\(error!) \(error!._userInfo)")
            }
        })
        
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
