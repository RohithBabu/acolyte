//
//  AddCategoryViewController.swift
//  FinalProject
//
//  Created by Moturu,Chaitanya Kiran on 4/26/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse
import Bolts

class AddCategoryViewController: UIViewController   {

    
    @IBOutlet weak var inputCategoryTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
   
    }
    
    
    
    @IBAction func addCategory(_ sender: Any) {
        let category1 = PFObject(className: "category")
        category1["categoryname"] = inputCategoryTF.text
        
        category1.saveInBackground(block: { (success, error) -> Void in
            print("question has been saved.")
        })
        inputCategoryTF.text = ""
        
        
    }
   
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    


}
