//
//  QuestionAndAnswersViewController.swift
//  FinalProject
//
//  Created by Moturu,Chaitanya Kiran on 4/10/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse
import Bolts

class QuestionAndAnswersViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var tableView: UITableView!
       var a=["sf","as"]
    var category:String = ""
    var bikes:[PFObject] = []
    var bikes1:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate=self
        tableView.dataSource=self
                 // Do any additional setup after loading the view.
            }
    override func viewWillAppear(_ animated: Bool) {
       
        let query = PFQuery(className: "categoryQ")
  
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                
                self.bikes = objects!
                self.bikes1.removeAll()
                for name in objects! {
                    
                    if (name["category"] as! String == self.category){
                        self.bikes1.append(name["categoryQuestion"] as! String)
                       print("---------------------\(self.bikes1.append(name["categoryQuestion"] as! String))")
                    }
                    
                }
                self.tableView.reloadData()
                
  
            }
            else {
                
            }
        })
       

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("asdfasdf\(bikes.count)")
        return bikes1.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=UITableViewCell()
        //print("---------\(indexPath.row)")
        print((bikes1[indexPath.row]))
        cell.textLabel?.text="\(bikes1[indexPath.row])"
        //print("----------------------\(a[indexPath.row])")
        //tableView.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let temp:String = "\(bikes1[indexPath.row])"
        performSegue(withIdentifier: "first", sender: temp)
    }
    override func prepare(for segue: UIStoryboardSegue, sender:Any? ) {
        let c=segue.destination as! DisplayQuestionWithAnswerViewController
        c.category = self.category
        c.categoryQuestion = sender as! String
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
