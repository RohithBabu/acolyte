//
//  RegisterPageViewController.swift
//  FinalProject
//
//  Created by Moturu,Chaitanya Kiran on 4/2/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse

class RegisterPageViewController: UIViewController {

    @IBOutlet weak var emailIdTf: UITextField!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var rePasswordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClickRegisterBt(_ sender: Any) {
        let user = PFUser()
        
        user.username = usernameTF.text! //usernameTF.text! user.password = passwordTF.text!
        user.email = emailIdTf.text!
        user.password = passwordTF.text!
        
        if passwordTF.text! != "" || rePasswordTF.text! != "" {
            if self.passwordTF.text! != self.rePasswordTF.text! {
                
                self.displayAlertWithTitle("Password mismatch", message: "password mismatch")
                
            } else {
                
                user.signUpInBackground( block: {
                    (success, error) -> Void in
                    if let error = error as NSError? {
                        let errorString = error.userInfo["error"] as? NSString
                        // In case something went wrong, use errorString to get the error
                        self.displayAlertWithTitle("Something has gone wrong", message:"\(errorString)")
                        
                    } else {
                        // Everything went okay
                        self.displayAlertWithTitle("Success!", message:"Registration successful")
                        
                        self.emailIdTf.text! = ""
                        self.passwordTF.text! = ""
                        self.rePasswordTF.text! = ""
                        self.usernameTF.text! = ""
                        
                        let emailVerified = user["emailVerified"]
                        if emailVerified != nil && (emailVerified as! Bool) == true {
                            // Everything is fine
                            PFUser.logOut()

                        } else {
                            // The email has not been verified, so logout the user
                            PFUser.logOut()
                        }
                    } })
            }
        } else {
            displayAlertWithTitle("Sorry", message: "Please Enter Details")
        }

    }
   
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
