//
//  SearchPageViewController.swift
//  FinalProject
//
//  Created by Moturu,Chaitanya Kiran on 4/2/17.
//  Copyright © 2017 Sadhu,Rohith Babu. All rights reserved.
//

import UIKit
import Parse

class SearchPageViewController: UIViewController {

         var questions:[PFObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var outputSeacrhedQuestions: UITextView!

    @IBOutlet weak var inputSearchWord: UITextField!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func searchForQuestion(_ sender: Any) {

        displayOutputAskedQuestions()
        
    }
    
   
        func displayOutputAskedQuestions(){
        self.outputSeacrhedQuestions.text = "Searched Questions :"
        let query = PFQuery(className:"categoryQ")
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                
                self.questions = objects!
                print(self.questions.count)
                // Do something with the found objects
                var countResult = 0
                for i in self.questions{
                    if (i["categoryQuestion"]! as! String).lowercased().range(of: self.inputSearchWord.text!) != nil {
                    self.outputSeacrhedQuestions.text =  self.outputSeacrhedQuestions.text + "\n" + "\(i["categoryQuestion"]!)"
                    print(i["categoryQuestion"])
                        countResult = countResult+1
                    }}

            } else {
                // Log details of the failure
                self.displayAlertWithTitle("Oops", message: "\(error!) \(error!._userInfo)")
            }
        })
        
    }//
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }

}
